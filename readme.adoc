= worldview
Ernest Bruce

:hardbreaks-option:

:humanespace:                https://gitlab.com/ernest_bruce/humanespace

:traits_dir:                 traits/
:rel_traits_dir:             ../{traits_dir}

:humanet_properties:         humanet_properties.adoc
:humanet_principles:         humanet_principles.adoc
:humanet_services:           humanet_services.adoc
:humanet_purpose:            humanet_purpose.adoc
:humanespace_properties:     humanespace_properties.adoc
:humanespace_principles:     humanespace_principles.adoc
:humanespace_purpose:        humanespace_purpose.adoc
:humanespace_services:       humanespace_services.adoc

:worldview_properties:       worldview_properties.adoc
:worldview_principles:       worldview_principles.adoc
:worldview_purpose:          worldview_purpose.adoc
:worldview_services:         worldview_services.adoc



*worldview* is the {humanespace}[humanespace·based] construct through which a person gives meaning to language and catagorizes information to make decisions and reach conclusions
&nbsp;&nbsp;


== properties
include::{traits_dir}{worldview_properties}[]
&nbsp;&nbsp;


== principles
include::{traits_dir}{worldview_principles}[]
&nbsp;&nbsp;


== services
include::{traits_dir}{worldview_services}[]
&nbsp;&nbsp;


== purpose
include::{traits_dir}{worldview_purpose}[]
